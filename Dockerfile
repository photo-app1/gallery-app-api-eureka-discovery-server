FROM openjdk:11
VOLUME /tmp
COPY build/libs/gallery-app-eureka-server-0.0.1-SNAPSHOT.jar GalleryAppApiDiscoveryServer.jar
ENTRYPOINT ["java","-jar","GalleryAppApiDiscoveryServer.jar"]